﻿namespace WebRecipe.Api.Models.Requests;

public class IdRequest
{
    public int Id { get; set; }
}
