﻿namespace WebRecipe.Api.Models.Requests;

public class NameRequest
{
    public string Name { get; set; } = null!;
}
