FROM mcr.microsoft.com/dotnet/sdk:6.0 as build-env

WORKDIR /src
COPY ./WebRecipe.Api/*.csproj .
RUN dotnet restore
COPY WebRecipe.Api .
COPY ./WebRecipe.Infrastructure ../WebRecipe.Infrastructure
RUN dotnet publish -c Release -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime
WORKDIR /publish
COPY --from=build-env /publish .
ENV DB_CONNECTION_STRING=""
EXPOSE 80
EXPOSE 443
ENTRYPOINT ["dotnet", "WebRecipe.Api.dll"]
